# TP Choco-Mining

Ce TP a pour but de pratiquer la fouille de motifs en modélisant des problèmes de ce type à l'aide de la librairie de Programmation Par Contraintes (PPC) Choco-Mining (https://gitlab.com/chaver/choco-mining). Choco-Mining repose sur le solveur Choco pour la modélisation et la résolution des modèles, dont la documentation est disponible à cette adresse: https://choco-solver.org/.

## Pré-requis

Choco-Mining nécessite d'avoir Java version 8 ou ultérieure installée sur votre ordinateur. De plus, il est très pratique d'avoir un environnement de développement (IDE) pour pouvoir écrire, vérifier et exécuter le code. Nous couvrons ici deux IDE populaires, Eclipse et Intellij Idea, mais il est possible d'utiliser l'IDE de votre choix pour réaliser le TP.

Ensuite, il existe deux possibilités pour utiliser la librairie: soit importer le fichier `.jar` de la librairie directement dans votre IDE, soit utiliser Maven pour télécharger et installer les dépendances requises.

### Option 1: Importer le fichier .jar dans votre IDE (méthode recommandée)

Le fichier `.jar` qui contient toutes les dépendances nécessaires pour le TP se trouve à cette adresse: https://repo1.maven.org/maven2/io/gitlab/chaver/choco-mining/1.0.2/choco-mining-1.0.2-jar-with-dependencies.jar.

Après avoir téléchargé le fichier, vous devez l'importer en tant que librairie dans votre IDE:

- Eclipse: https://tech.bragboy.com/2016/01/quick-way-to-import-jar-in-eclipse.html
- Intellij Idea: https://www.geeksforgeeks.org/how-to-add-external-jar-file-to-an-intellij-idea-project/

### Option 2: Utiliser Maven

Si vous êtes familier avec Maven, vous pouvez l'utiliser pour télécharger et installer les dépendances requises à l'aide du fichier `pom.xml`.

### Tester son installation

Après avoir effectué les instructions ci-dessus, vous devez vérifier que la librairie a été importée avec succès dans votre IDE. Pour cela, vous pouvez exécuter la classe `FrequentItemsetMining` qui se trouve dans le dossier `src/main/java/org/example`. Si la console vous affiche un message dont la fin ressemble à ça

```
- Complete search - 177 solution(s) found.
	Model[Frequent Itemset Mining]
	Solutions: 177
	Building time : 0.128s
	Resolution time : 0.109s
	Nodes: 353 (3,236.1 n/s) 
	Backtracks: 353
	Backjumps: 0
	Fails: 0
	Restarts: 0
```

Cela signifie que la librairie a été importée correctement. Sinon, vous avez probablement obtenu un message d'erreur et dans ce cas, il faut vérifier à nouveau votre installation.

## Background sur la fouille de motifs

Au cours de ce TP, nous allons travailler sur des bases de données dites **transactionnelles**. Une base de données transactionnelle repose sur deux concepts clés:

- Un ensemble d'items $\mathcal{I} = \{1,...,n\}$
- Un ensemble de transactions $\mathcal{T} = \{t_1,...,t_m\}$, où chaque transaction $t_i$ est un sous-ensemble de $\mathcal{I}$

Chaque base de données transactionnelle est stockée dans un fichier avec l'extension `.dat`, où chaque ligne du fichier représente une transaction. Nous pouvons illustrer cela en prenant par exemple la base de données iris qui est stockée dans le fichier `data/iris.dat`. Les deux premières lignes du fichier sont les suivantes:

```
1 4 9 10 13
1 4 8 10 13
```

La 1ère ligne représente la transaction $t_1$ de la base de données. La transaction $t_1$ contient 5 items: 1, 4, 9, 10 et 13. La 2ème ligne représente la transaction $t_2$, qui contient 5 items: 1, 4, 8, 10 et 13. Nous pouvons appliquer la même logique pour les autres lignes du fichier. En tout, la base de données iris contient 150 transactions.

L'objectif de la fouille de motifs est comme son nom l'indique d'extraire des motifs intéressants dans une base de données transactionnelle. On appelle **motif** une régularité qui apparaît dans la base et qui est susceptible d'intéresser l'utilisateur. Il existe plusieurs types de motifs mais dans ce TP nous allons nous intéresser exclusivement à un type de motif précis: les **itemsets**.

Un itemset est comme son nom l'indique un sous-ensemble d'items. Par exemple, $\{1,4,9\}$ est un itemset qui contient les items 1, 4 et 9.  On dit qu'un itemset apparaît dans une transaction si c'est un sous-ensemble de cette transaction. Par exemple, $\{1,4,9\}$ apparaît dans la transaction $t_1$ de l'exemple précédent mais pas dans $t_2$.

Le nombre potentiel de motifs qu'il est possible d'extraire d'une base de données est exponentiel (i.e. $2^n - 1$), il est donc irréaliste de vouloir tous les extraire. De plus, l'utilisateur final dispose généralement d'un temps limité pour analyser les motifs extraits, il faut donc trouver un moyen efficace afin d'extraire un nombre limité de motifs qui sont susceptibles d'intéresser l'utilisateur.

Une première possibilité pour limiter le nombre de motifs est de se concentrer uniquement sur les motifs **fréquents**. La **fréquence** d'un motif $x$ (notée $freq(x)$) représente le nombre de transactions dans lequel il apparaît. L'objectif est alors d'extraire tous les motifs tel que $freq(x) \ge \theta$, où $\theta$ est un seuil fixé par l'utilisateur. 

Cependant, le nombre de motifs fréquents reste généralement trop élevé pour mettre à l'utilisateur de tous les analyser. Une deuxième possibilité pour limiter le nombre de motifs est le principe de **représentation condensée**. On dit qu'un ensemble de motifs $A$ est une représentation condensée d'un ensemble de motifs $B$ ssi deux conditions sont respectées:

- $A$ est un sous-ensemble de $B$
- Il est possible de déduire les informations manquantes à propos de tous les motifs qui appartiennent à $B$ mais pas à $A$

Dans ce TP, nous allons nous intéresser à un type de représentation condensée spécifique, les **motifs clos**. On dit qu'un motif $x$ est clos par rapport à la fréquence si il n'a pas de super ensemble $y$ tel que $freq(x) = freq(y)$. Ainsi, on dit que l'ensemble des motifs clos par rapport à la fréquence forme une représentation condensée de tous les motifs fréquents. En effet, si un motif $x$ n'est pas clos par rapport à la fréquence, il est possible de retrouver sa fréquence en cherchant son plus petit super-ensemble $y$ dans les motifs clos et nous avons la garantie que $freq(x) = freq(y)$.

Bien que les motifs clos par rapport à la fréquence soient intéressants du point de vue de l'utilisateur, il existe également des situations où ce dernier est intéressé par d'autres mesures que la fréquence. Nous allons voir ici quatre nouvelles mesures qui seront utilisées au cours du TP, la taille, l'aire, la fréquence maximum et la all-confidence:

- La taille d'un motif $x$, notée $taille(x)$, représente le nombre d'items que contient le motif. Par exemple, $taille(\{1,4,9\}) = 3$.
- L'aire d'un motif $x$, notée $aire(x)$, se calcule de la façon suivante: $aire(x) = freq(x) \times taille(x)$. L'aire représente le volume total occupé par un motif dans la base de données.
- La fréquence maximum d'un motif $x$, notée $max(x.freq)$, représente la fréquence maximum des items qu'il contient, i.e. $max(x.freq) = max \{freq(i) ~|~ i \in x\}$. Par exemple, supposons que $freq(\{1\}) = 3$, $freq(\{4\}) = 5$ et $freq(\{9\}) = 6$. Dans ce cas, nous avons $max(\{1,4,9\}.freq) = max(freq(\{1\}),freq(\{4\}),freq(\{9\})) = freq(\{9\}) = 6$
- La all-confidence d'un motif $x$, notée $aconf(x)$, se calcule de la façon suivante: $aconf(x) = \frac{freq(x)}{max(x.freq)}$. Par exemple, supposons que $freq(\{1,4,9\}) = 3$, $freq(\{1\}) = 3$, $freq(\{4\}) = 5$ et $freq(\{9\}) = 6$. Dans ce cas, nous avons $aconf(\{1,4,9\}) = \frac{freq(\{1,4,9\})}{max(\{1,4,9\}.freq)} = \frac{freq(\{1,4,9\})}{freq(\{9\})} = 0.5$.

Il est ainsi possible d'étendre la notion de motif clos à plusieurs mesures. Étant donné un ensemble de mesures $M$, nous disons que $x$ est clos par rapport à $M$ ssi il n'existe pas de motif $y$ qui est un super-ensemble de $x$ tel que $\forall m \in M : m(x) = m(y)$. En d'autres termes, si il existe un motif $y$ dont toutes les valeurs des mesures de $M$ sont égales à celles de $x$, alors $x$ n'est pas clos par rapport à $M$.

Une fois de plus, les motifs clos par rapport à $M$ forme une représentation condensée de l'ensemble de tous les motifs. Il est en effet possible de déduire les valeurs de chaque mesure $m \in M$ pour n'importe quel motif non-clos $x$ en prenant son plus petit super-ensemble $y$ dans les motifs clos et en déduisant $m(x) = m(y)$.

Enfin, bien que le concept de motif clos soit utile pour réduire le nombre total de motifs extraits, ce n'est pas toujours suffisant pour obtenir un nombre raisonnable de motifs. De plus, il est nécessaire de fixer le seuil de fréquence minimum $\theta$, ce qui n'est pas évident à première vue pour l'utilisateur: en fixant le seuil trop bas, on risque d'avoir une explosion du nombre de motifs, et en fixant trop haut, on risque de rater de nombreux motifs intéressants.

Dans ce TP, nous allons nous intéresser à la dominance de Pareto afin d'obtenir un petit nombre de motifs intéressants pour l'utilisateur sans avoir à fixer de seuil. Étant donné un ensemble de mesures $M$, nous disons qu'un motif $x$ domine un motif $y$ ssi $\forall m \in M: m(x) \ge m(y)$ et $\exists m \in M : m(x) > m(y)$. On appelle **skypattern** un motif qui n'est dominé par aucun autre motif de la base de données. L'objectif de la fouille de skypatterns est ainsi d'extraire tous les skypatterns d'une base de données. Le nombre de skypatterns étant très réduit comparé aux autres types de motifs, il est généralement plus facile de les extraire et de les analyser.

Il est également possible d'extraire des corrélations entre deux motifs, intitulées **règles d'associations**. Étant donné deux motifs $x$ et $y$, on note $x \Rightarrow y$ la règle d'association qui indique que la présence de $x$ implique également celle de $y$. Il existe plusieurs mesures d'intérêt pour qualifier l'intérêt d'une règle d'association:

- La **fréquence**: notée $freq(x \Rightarrow y)$, représente le nombre de transactions de la base où la règle apparaît, i.e. $freq(x \Rightarrow y) = freq(x \cup y)$
- La **confiance**: notée $conf(x \Rightarrow y)$, représente le pourcentage de transactions qui contiennent le motif $x \cup y$ parmi celles qui contiennent le motif $x$, i.e. $conf(x \Rightarrow y) = \frac{freq(x \cup y)}{freq(x)}$. La valeur de la confiance est située entre 0 et 1, plus la valeur est élevée, plus la probabilité de trouver la règle vraie dans la base augmente.
- Le **lift**: noté $lift(x \Rightarrow y)$. La mesure confiance peut parfois être trompeuse, car une règle avec une confiance élevée ne signifie pas forcément que la présence de $x$ implique celle de $y$. Prenons par exemple une base de données qui contient deux items, un ordinateur (noté $O$) et un antivirus (noté $A$) et qui représente les ventes d'une entreprise high-tech (i.e. chaque transaction représente une vente). Supposons que $|\mathcal{D}| = 10000$ (i.e. 10000 ventes), $freq(O) = 9000$, $freq(A)= 8000$, $freq(\{O,A\}) = 7000$. Nous avons $conf(A \Rightarrow O) = \frac{7}{8}$, ce qui semble indiquer que les personnes qui achètent un antivirus sont susceptibles d'acheter un ordinateur. Cependant, nous avons $conf(\emptyset \Rightarrow O) = \frac{9}{10} > conf(A \Rightarrow O)$. En d'autres termes, les personnes qui n'achètent pas d'antivirus sont plus susceptibles d'acheter un antivirus que celles qui n'en achètent pas. Le lift permet d'éviter des informations trompeuses de la sorte. Il se calcule de la façon suivante: $lift(x \Rightarrow y) = \frac{conf(x \Rightarrow y) \times |\mathcal{D}|}{freq(y)}$. Sa valeur s'interprète comme suit:
  - si $lift(x \Rightarrow y) < 1$, alors cela signifie qu'il y a une corrélation négative entre $x$ et $y$ (i.e. la présence de $x$ inhibe celle de $y$). C'est le cas dans l'exemple présenté ci-dessus.
  - si $lift(x \Rightarrow y) = 1$, alors cela signifie que les deux motifs sont indépendants
  - si $lift(x \Rightarrow y) > 1$, alors il y a une corrélation positive entre $x$ et $y$

 

## Background sur la Programmation Par Contraintes (PPC)

Dans ce TP, nous allons faire de la fouille de motifs à l'aide d'un paradigme de programmation particulier appelé la Programmation Par Contraintes (PPC). La PPC permet de résoudre des problèmes complexes en spécifiant des modèles qui sont décomposés en un triplet $(X,D,C)$:

- $X = \{x_1,...,x_n\}$: ensemble des variables du problème
- $D = \{D_1,...,D_n\}$: ensemble des domaines des variables du problème, i.e. $x_i \in D_i$
- $C$: ensemble des contraintes du problème, où chaque contrainte spécifie des combinaisons de valeurs qui sont autorisées pour des ensembles de variables.

L'objectif de la résolution d'un modèle PPC est de trouver toutes les solutions qui respectent les contraintes spécifiées. Une solution consiste un assignement de valeur $v$ à chaque variable $x_i$ tel que $v \in D_i$.

Prenons par exemple le modèle PPC suivant:

- $X = \{x_1,x_2,x_3\}$
- $D_1 = D_2=D_3 = \{1,2,3\}$
- $C = \{C_1,C_2\}$ où
  - $C_1 \equiv x_1 \le x_2$
  - $C_2 \equiv x_2 < x_3$

Il existe 4 solutions à ce modèle PPC:

- $\{x_1=1,x_2=1,x_3=2\}$
- $\{x_1=1,x_2=1,x_3=3\}$
- $\{x_1=1,x_2=2,x_3=3\}$
- $\{x_1=2,x_2=2,x_3=3\}$

Ces dernières années, de nombreux travaux ont été consacrés à la modélisation de problèmes de fouille de motifs à l'aide de la PPC. La PPC présente un avantage indéniable pour la fouille de motifs: l'utilisateur peut facilement ajouter des contraintes personnalisées sans avoir à modifier le système sous-jacent. Par exemple, l'utilisateur peut spécifier que certains items doivent apparaître dans le motif recherché. Il est également possible de spécifier des contraintes plus complexes, ce que nous verrons au cours du TP.

## Un premier modèle pour l'extraction des itemsets fréquents

Dans cette première partie, nous allons chercher à extraire tous les itemsets fréquents dans une base de données transactionnelles. Nous allons nous intéresser à la base de données `iris` qui est bien connue dans le domaine de la fouille d'itemsets. Le code est disponible dans le fichier `src/main/java/org/example/FrequentItemsetMining.java`. Nous allons analyser ensemble le code de la méthode `main`:

```java
String dataPath = "data/iris.dat";
TransactionalDatabase database = new DatReader(dataPath).read();
```

Tout d'abord, nous commençons par lire la base de données située dans le fichier `data/iris.dat` et nous stockons le résultat dans une variable nommée `database`. Le format du fichier est le suivant:

```
1 4 9 10 13 
1 4 8 10 13
```

La première transaction contient 5 items: 1, 4, 9, 10 et 13. La deuxième transaction contient également 5 items: 1, 4, 8, 10 et 13, etc...

```java
int theta = 10;
```

La variable `theta` contient la valeur de la fréquence minimum des itemsets: ici, nous voulons extraire tous les itemsets qui ont une fréquence minimale de 10.

```java
Model model = new Model("Frequent Itemset Mining");
// Array of Boolean variables where x[i] is true iff item i belongs to the itemset
BoolVar[] x = model.boolVarArray("x", database.getNbItems());
// Integer variable that represents the frequency of x
IntVar freq = model.intVar("freq", theta, database.getNbTransactions());
```

Nous créons ensuite un modèle puis un tableau de variables Booléennes `x`qui représente l'itemset recherché puis une variable entière `freq` qui représente la fréquence de l'itemset. La variable `freq` a une borne inférieure égale à `theta` et une borne supérieure égale au nombre de transactions de la base de données.

```java
// CoverSize is a constraint that links the freq variable to the frequency of x
ConstraintFactory.coverSize(database, freq, x).post();
```

Nous postons une contrainte `CoverSize` qui lie la variable `freq` à la fréquence de `x`.

```java
// Search Strategy: select item i such that freq(x U {i}) is minimal and instantiate it first to 0
Solver solver = model.getSolver();
solver.setSearch(Search.intVarSearch(
        new MinCov(model, database),
        new IntDomainMin(),
        x
));
```

Nous spécifions ensuite l'heuristique de recherche pour faciliter cette dernière.

```java
System.out.println("List of frequent itemsets for the dataset " + dataPath + " with a frequency threshold=" + theta + ":");
while (solver.solve()) {
    int[] itemset = IntStream
            .range(0, database.getNbItems())
            .filter(i -> x[i].getValue() == 1)
            .map(i -> database.getItems()[i])
            .toArray();
    System.out.println(Arrays.toString(itemset) + ", frequency=" + freq.getValue());
}
System.out.println("______________________");
solver.printStatistics();
```

Enfin, nous affichons chaque itemset fréquent dans la console ainsi que des statistiques sur la recherche. Les statistiques ont le format suivant:

```
** Choco 4.10.13 (2023-06) : Constraint Programming Solver, Copyright (c) 2010-2023
- Model[Frequent Itemset Mining] features:
	Variables : 16
	Constraints : 1
	Building time : 0.039s
	User-defined search strategy : yes
	Complementary search strategy : no
- Complete search - 156 solution(s) found.
	Model[Frequent Itemset Mining]
	Solutions: 156
	Building time : 0.039s
	Resolution time : 0.047s
	Nodes: 311 (6,570.2 n/s) 
	Backtracks: 311
	Backjumps: 0
	Fails: 0
	Restarts: 0
```

Ici, nous avons découvert 156 itemsets fréquents (nombre de solutions).

## Un deuxième modèle pour l'extraction des itemsets clos par rapport à la fréquence

Le nombre d'itemsets fréquents reste relativement élevé (156). Comment pouvons-nous réduire ce nombre tout en conservant les itemsets les plus intéressants ? Une première possibilité est d'utiliser le principe de clôture: nous disons qu'un itemset `x` est clos par rapport à la fréquence si il n'existe pas de motif `y` tel que `x` est un sous-ensemble de `y` et `freq(x) = freq(y)`. Ainsi, nous disons que l'ensemble des motifs clos par rapport à la fréquence forme une représentation condensée de l'ensemble des motifs fréquents. En effet, si un motif `x` donné n'est pas clos, il est possible de connaître sa fréquence en recherchant son plus petit super-ensemble `y` et nous avons `freq(x) = freq(y)`.

Le fichier `src/main/java/org/example/ClosedItemsetMining.java` contient le code (incomplet) pour extraire l'ensemble des motifs clos qui ont une fréquence minimale de 10. Le code est totalement identique à celui de l'exercice précédent, il manque simplement une ligne pour garantir que le motif est clos.

**Travail**: Compléter le code pour garantir que le motif `x` est clos par rapport à la fréquence. Pour cela, il faut explorer la documentation de Choco-Mining (https://gitlab.com/chaver/choco-mining/-/wikis/home) et trouver la bonne contrainte à poster. Que constatez-vous par rapport aux nombre de motifs extraits en le comparant au nombre de motifs extraits dans l'exercice précédent ?

## Un troisième modèle pour l'extraction des motifs clos par rapport à plusieurs mesures

Il est également possible de garantir qu'un motif est clos par rapport à plusieurs mesures à la fois (et pas seulement la fréquence). Nous disons qu'un motif $x$ est clos par rapport à un ensemble de mesures $M$ ssi il n'existe pas de super-ensemble $y$ de $x$ tel que $m(x) = m(y)$ pour chaque mesure $m \in M$.

Le fichier `src/main/java/org/example/ClosedMeasuresItemsetMining.java` contient le code (incomplet) pour extraire l'ensemble des motifs clos par rapport à l'ensemble de mesures $M = \{freq(x), max(x.freq)\}$. 

**Travail**: Compléter le code pour garantir que le motif $x$ est clos par rapport à l'ensemble de mesures $M$. Nous pouvons procéder en deux temps:

1. Trouver la contrainte pour calculer la variable $maxFreq$ qui représente la valeur de $max(x.freq)$. *Indice*: Choco fournit une contrainte $max$ qui permet de lier une variable donnée à la valeur maximum des variables d'un tableau donné, explorez la documentation de Choco afin de la découvrir: https://choco-solver.org/.
2. Trouver la bonne contrainte à poster pour garantir qu'un motif $x$ est clos par rapport à un ensemble de mesures (se trouve dans la documentation de Choco-Mining).

Que constatez-vous en comparant le nombre de motifs extraits dans cet exercice et dans l'exercice précédent ? Savez-vous expliquer pourquoi ?

## Un quatrième modèle pour l'extraction des skypatterns

Nous allons à présenter nous intéresser aux motifs skypatterns. Ces motifs ne nécessitent pas de seuil pour être extraits et ils représentent de façon implicite les préférences de l'utilisateur: si un motif $x$ est meilleur qu'un motif $y$ sur toutes les mesures d'un ensemble $M$ donné, alors $x$ sera toujours préféré à $y$ par l'utilisateur. 

Le fichier `src/main/java/org/example/SkypatternMining.java` contient le code (incomplet) pour réaliser cette tâche avec le dataset Iris.

**Travail**: Compléter le code pour extraire tous les motifs skypatterns, et pour chaque skypattern, affichez le dans la console ainsi que sa fréquence, son aire et son aconf.

**Indices**: Il est possible d'extraire les skypatterns à l'aide de la méthode `findParetoFront()` du solver, qui retourne une liste de `Solution`, où chaque `Solution` représente un skypattern. Ensuite, il faudra parcourir la liste de ces solutions afin de récupérer la valeur de chaque variable qui nous intéresse. La classe `Solution` a une méthode `getIntVal()` qui permet de récupérer la valeur d'une variable passée en paramètre. 

Que constatez-vous en comparant le nombre de motifs skypatterns par rapport au nombre de motifs extraits lors des exercices précédents ?

## Un cinquième modèle pour l'extraction des règles à partir des skypatterns

Dans ce dernier exercice, nous allons nous intéresser à l'extraction de règles à partir des skypatterns. En effet, les skypatterns par rapport à l'ensemble de mesures $M = \{freq(x),aire(x),aconf(x)\}$ sont intéressants pour deux raisons:

1. Ils maximisent la fréquence, ce qui signifie que les règles générées auront également une fréquence élevée
2. Ils maximisent l'aconf, qui une borne inférieure de la confiance des règles générées à partir du skypattern. Ainsi, si $z$ est un skypattern, nous avons la garantie que toute règle $x \Rightarrow y$ générée à partir de $z$ respectera la propriété suivante: $conf(x \Rightarrow y) \ge aconf(z)$

**Travail n°1**: Dans le fichier `src/main/java/org/example/AssociationRuleMining.java`, copier-coller le code de la méthode main de la classe `SkypatternMining` dans la méthode `extractSkypatterns()`. Modifiez le code de cette méthode pour qu'elle retourne une liste d'ensemble d'entier, où chaque ensemble d'entiers représente un skypattern. (*Indice*: Pour chaque indice $i$ allant de 0 à `database.getNbItems()`, vérifier si la valeur de `x[i]` est égale à 1, si c'est le cas, ajouter $i$ à l'ensemble).

**Travail n°2**: Exécuter la méthode `modelToExtractRules()` qui extrait les règles d'association en utilisant les skypatterns extraits par la méthode `extractSkypatterns()`. Selon vous, quelles sont les règles les plus intéressantes ? Pourquoi ?

**Travail n°3**: Nous souhaitons à présent extraire uniquement des règles qui contiennent au moins un item de classe. Ici, les items de classe sont à à l'index 0, 1 et 2. Trouvez comment imposer une contrainte qui garantit la présence d'au moins un item de classe dans le skypattern. Puis exécutez à nouveau la méthode `modelToExtractRules()`. Cette fois-ci, quelles sont les règles les plus intéressantes ?

**Travail n°4**: Nous souhaitons à présent garantir la présence d'au moins un item de classe dans la partie conséquence de la règle $y$. Modifiez la méthode `modelToExtractRules()` afin d'obtenir le résultat attendu. 
