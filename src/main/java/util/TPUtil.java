package util;

import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.BoolVar;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.IntStream;

public class TPUtil {

    public static String[] readLabels(String path) throws IOException {
        return Files.readAllLines(Paths.get(path)).stream().toArray(String[]::new);
    }

    public static String[] convertItemsetToLabel(String[] labels, BoolVar[] x) {
        return IntStream
                .range(0, labels.length)
                .filter(i -> x[i].getValue() == 1)
                .mapToObj(i -> labels[i])
                .toArray(String[]::new);
    }

    public static String[] convertItemsetToLabel(String[] labels, BoolVar[] x, Solution solution) {
        return IntStream
                .range(0, labels.length)
                .filter(i -> solution.getIntVal(x[i]) == 1)
                .mapToObj(i -> labels[i])
                .toArray(String[]::new);
    }
}
