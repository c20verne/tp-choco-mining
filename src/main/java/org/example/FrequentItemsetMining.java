package org.example;

import io.gitlab.chaver.mining.patterns.constraints.factory.ConstraintFactory;
import io.gitlab.chaver.mining.patterns.io.DatReader;
import io.gitlab.chaver.mining.patterns.io.TransactionalDatabase;
import io.gitlab.chaver.mining.patterns.search.strategy.selectors.variables.MinCov;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import java.util.Arrays;
import static util.TPUtil.*;

/**
 * Frequent Itemset Mining : the goal is to find all the itemsets that have a frequency greater than a given theta threshold
 */
public class FrequentItemsetMining {

    public static void main(String[] args) throws Exception {
        String[] irisLabels = readLabels("data/iris.names");
        String dataPath = "data/iris.dat";
        TransactionalDatabase database = new DatReader(dataPath).read();
        int theta = 10;
        Model model = new Model("Frequent Itemset Mining");
        // Array of Boolean variables where x[i] is true iff item i belongs to the itemset
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        // Integer variable that represents the frequency of x
        IntVar freq = model.intVar("freq", theta, database.getNbTransactions());
        // CoverSize is a constraint that links the freq variable to the frequency of x
        ConstraintFactory.coverSize(database, freq, x).post();
        // Search Strategy: select item i such that freq(x U {i}) is minimal and instantiate it first to 0
        Solver solver = model.getSolver();
        solver.setSearch(Search.intVarSearch(
                new MinCov(model, database),
                new IntDomainMin(),
                x
        ));
        System.out.println("List of frequent itemsets for the dataset " + dataPath + " with a frequency threshold=" + theta + ":");
        while (solver.solve()) {
            String[] itemset = convertItemsetToLabel(irisLabels, x);
            System.out.println("x=" + Arrays.toString(itemset) + ", freq(x)=" + freq.getValue());
        }
        System.out.println("______________________");
        solver.printStatistics();
    }
}
