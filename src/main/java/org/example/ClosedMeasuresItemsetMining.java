package org.example;

import io.gitlab.chaver.mining.patterns.constraints.factory.ConstraintFactory;
import io.gitlab.chaver.mining.patterns.io.DatReader;
import io.gitlab.chaver.mining.patterns.io.TransactionalDatabase;
import io.gitlab.chaver.mining.patterns.measure.Measure;
import io.gitlab.chaver.mining.patterns.search.strategy.selectors.variables.MinCov;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import java.util.Arrays;
import java.util.List;

import static util.TPUtil.*;
import static io.gitlab.chaver.mining.patterns.measure.MeasureFactory.*;

/**
 * Closed Itemset Mining : the goal is to find all the itemsets that have a frequency greater than a given theta threshold
 * and that are closed with respect to several measures.
 * We say that an itemset x is closed with respect to a set of measures M iff there exists no superset y such that
 * m(x) = m(y) for all the measure m in M
 */
public class ClosedMeasuresItemsetMining {

    public static void main(String[] args) throws Exception {
        String[] irisLabels = readLabels("data/iris.names");
        String dataPath = "data/iris.dat";
        int theta = 10;
        Model model = new Model("Closed Measures Itemset Mining");
        TransactionalDatabase database = new DatReader(dataPath).read();
        // Array of Boolean variables where x[i] is true iff item i belongs to the itemset
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        // Integer variable that represents the frequency of x
        IntVar freq = model.intVar("freq", theta, database.getNbTransactions());
        // CoverSize is a constraint that links the freq variable to the frequency of x
        ConstraintFactory.coverSize(database, freq, x).post();
        // itemFreq[i] is an array of integer variables such that:
        // - itemFreq[i] = freq(i) if x[i] = 1
        // - itemFreq[i] = 0 if x[i] = 0
        IntVar[] itemFreq = model.intVarArray(x.length, 0, database.getNbTransactions());
        int[] freqItems = database.computeItemFreq();
        for (int i = 0; i < x.length; i++) {
            itemFreq[i].eq(x[i].mul(freqItems[i])).post();
        }
        // Integer variable that represents max(x.freq)
        IntVar maxFreq = model.intVar("maxFreq", 0, database.getNbTransactions());
        // Post a constraint to ensure that maxFreq = max(x.freq)

        // Post the constraint to ensure that x is a closed itemset w.r.t. M = {freq(x),max(x.freq)}
        List<Measure> measures = Arrays.asList(freq(),maxFreq());

        // Set up the search strategy
        Solver solver = model.getSolver();
        solver.setSearch(Search.intVarSearch(
                new MinCov(model, database),
                new IntDomainMin(),
                x
        ));
        System.out.println("List of closed itemsets w.r.t. M = {freq(x),max(x.freq)} for the dataset " + dataPath + " with a frequency threshold=" + theta + ":");
        while (solver.solve()) {
            String[] itemset = convertItemsetToLabel(irisLabels, x);
            System.out.println("x=" + Arrays.toString(itemset) + ", freq(x)=" + freq.getValue() + ", max(x.freq)=" + maxFreq.getValue());
        }
        System.out.println("______________________");
        solver.printStatistics();
    }
}
