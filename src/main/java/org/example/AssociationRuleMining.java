package org.example;

import io.gitlab.chaver.mining.patterns.constraints.factory.ConstraintFactory;
import io.gitlab.chaver.mining.patterns.io.DatReader;
import io.gitlab.chaver.mining.patterns.io.TransactionalDatabase;
import io.gitlab.chaver.mining.patterns.measure.Measure;
import io.gitlab.chaver.mining.patterns.measure.operand.MeasureOperand;
import io.gitlab.chaver.mining.patterns.search.strategy.selectors.variables.MinCov;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.expression.discrete.relational.ReExpression;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.search.strategy.selectors.variables.InputOrder;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.*;

import static io.gitlab.chaver.mining.patterns.measure.MeasureFactory.*;
import static org.chocosolver.solver.search.strategy.Search.intVarSearch;
import static util.TPUtil.*;

public class AssociationRuleMining {

    /**
     * Given an itemset and a transactional database, returns the associated closed itemset
     * @param itemset
     * @param database
     * @return
     */
    public static Set<Integer> findClosedItemset(Set<Integer> itemset, TransactionalDatabase database) {
        BitSet cover = new BitSet(database.getNbTransactions());
        cover.set(0, database.getNbTransactions());
        for (int i : itemset) {
            cover.and(database.getVerticalRepresentation()[i]);
        }
        Set<Integer> closure = new HashSet<>();
        for (int i = database.getNbClass(); i < database.getNbItems(); i++) {
            BitSet temp = (BitSet) cover.clone();
            temp.and(database.getVerticalRepresentation()[i]);
            if (temp.cardinality() == cover.cardinality()) {
                closure.add(i);
            }
        }
        return closure;
    }

    /**
     * Associate each itemset to its closure
     * @return a map which associates each itemset to its closure (the key is the itemset and the value its closure)
     */
    public static Map<Set<Integer>, Set<Integer>> getClosedItemsets(List<Set<Integer>> itemsets, TransactionalDatabase database) {
        Map<Set<Integer>, Set<Integer>> closedItemsets = new HashMap<>();
        for (Set<Integer> skypattern : itemsets) {
            Set<Integer> closedItemset = findClosedItemset(skypattern, database);
            closedItemsets.put(skypattern, closedItemset);
        }
        return closedItemsets;
    }

    /**
     * Make the union of all the itemsets in the list
     * @param itemsets
     * @return
     */
    public static Set<Integer> itemsetsUnion(List<Set<Integer>> itemsets) {
        Set<Integer> union = new HashSet<>();
        itemsets.forEach(union::addAll);
        return union;
    }

    /**
     * The goal is to impose a constraint such that each rule x => y is a subset of a skypattern
     * @param y
     * @param z
     * @param skypatterns
     * @param database
     * @param model
     * @return
     */
    public static BoolVar[] skypatternConstraint(BoolVar[] y, BoolVar[] z, List<Set<Integer>> skypatterns, TransactionalDatabase database, Model model) {
        Set<Integer> patternsUnion = itemsetsUnion(skypatterns);
        Map<Set<Integer>, Set<Integer>> closedItemsets = getClosedItemsets(skypatterns, database);
        BoolVar[] skyVar = new BoolVar[closedItemsets.size()];
        for (int i = 0; i < database.getNbItems(); i++) {
            if (!patternsUnion.contains(i)) {
                model.arithm(z[i], "=", 0).post();
            }
        }
        int i = 0;
        //System.out.println(closedPatterns);
        for (Map.Entry<Set<Integer>, Set<Integer>> entry : closedItemsets.entrySet()) {
            ReExpression sky = null;
            Set<Integer> skypattern = entry.getKey();
            Set<Integer> closedPattern = entry.getValue();
            Set<Integer> itemsOnlyInClosure = new HashSet<>(closedPattern);
            itemsOnlyInClosure.removeAll(skypattern);
            for (int item : patternsUnion) {
                ReExpression temp;
                int idx = item;
                if (!closedPattern.contains(item)) {
                    temp = z[idx].eq(0);
                }
                else if (itemsOnlyInClosure.contains(item)) {
                    temp = y[idx].eq(1);
                }
                else {
                    temp = z[idx].eq(1);
                }
                sky = sky == null ? temp : sky.and(temp);
            }
            skyVar[i] = sky.boolVar();
            i++;
        }
        model.sum(skyVar, ">=", 1).post();
        return skyVar;
    }

    public static List<Set<Integer>> extractSkypatterns() throws Exception {
        // Replace this with the code of the file "Skypattern Mining" after completing it
        // The method should return a list, where each element of the list is a skypattern
        String[] irisLabels = readLabels("data/iris.names");
        String dataPath = "data/iris.dat";
        Model model = new Model("Skypattern Mining");
        TransactionalDatabase database = new DatReader(dataPath).read();
        // Array of Boolean variables where x[i] is true iff item i belongs to the itemset
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        // Integer variable that represents the frequency of x
        IntVar freq = model.intVar("freq", 1, database.getNbTransactions());
        // CoverSize is a constraint that links the freq variable to the frequency of x
        ConstraintFactory.coverSize(database, freq, x).post();
        // Integer variable that represents the length (also called size) of x
        IntVar length = model.intVar("length", 2, database.getNbItems());
        // Post a constraint sum to ensure that length(x) = sum(x_i)
        model.sum(x, "=", length).post();
        // Integer variable that represents the area of x
        IntVar area = freq.mul(length).intVar();
        // itemFreq[i] is an array of integer variables such that:
        // - itemFreq[i] = freq(i) if x[i] = 1
        // - itemFreq[i] = 0 if x[i] = 0
        IntVar[] itemFreq = model.intVarArray(x.length, 0, database.getNbTransactions());
        int[] freqItems = database.computeItemFreq();
        for (int i = 0; i < x.length; i++) {
            itemFreq[i].eq(x[i].mul(freqItems[i])).post();
        }
        // Integer variable that represents max(x.freq)
        IntVar maxFreq = model.intVar("maxFreq", 0, database.getNbTransactions());
        // Post a constraint to ensures that maxFreq = max(itemFreq)
        model.max(maxFreq, itemFreq).post();
        // We compute the value of aconf(x) = freq(x) / max(x.freq)
        IntVar aconf = freq.mul(10000).div(maxFreq).intVar();
        // Now we are going to post two constraints
        List<Measure> M = Arrays.asList(freq(), area(), allConf());
        // First, we need to post a constraint to ensures that x is closed w.r.t. M', where M' is a set of measures such that M is maximally M'-skylineable
        Set<Measure> M_prime = MeasureOperand.maxConvert(M);
        ConstraintFactory.adequateClosure(database, new ArrayList<>(M_prime), x, false).post();
        // Set up the search strategy
        Solver solver = model.getSolver();
        solver.setSearch(Search.intVarSearch(
                new MinCov(model, database),
                new IntDomainMin(),
                x
        ));
        List<Solution> solutions =  solver.findParetoFront(new IntVar[]{freq, area, aconf}, true);
        List<Set<Integer>> skypatterns = new ArrayList<>();
        for (Solution sol : solutions) {
            Set<Integer> skypattern = new HashSet<>();
            for (int i = 0; i < database.getNbItems(); i++) {
                if (sol.getIntVal(x[i]) == 1) {
                    skypattern.add(i);
                }
            }
            skypatterns.add(skypattern);
        }
        return skypatterns;
    }


    /**
     * Extract association rules x => y from skypatterns
     * @throws Exception
     */
    public static void modelToExtractRules() throws Exception {
        List<Set<Integer>> skypatterns = extractSkypatterns();
        String[] irisLabels = readLabels("data/iris.names");
        String dataPath = "data/iris.dat";
        Model model = new Model("Association Rule Mining");
        TransactionalDatabase database = new DatReader(dataPath).read();
        // Antecedent of the rule : x
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        // Consequence of the rule : y
        BoolVar[] y = model.boolVarArray("y", database.getNbItems());
        // z = x U y
        BoolVar[] z = model.boolVarArray("z", database.getNbItems());
        for (int i = 0; i < database.getNbItems(); i++) {
            // Ensure that an item i is not in the antecedent and consequent of the rule at the same time
            model.arithm(x[i], "+", y[i], "<=", 1).post();
            // z[i] = x[i] OR y[i]
            model.addClausesBoolOrEqVar(x[i], y[i], z[i]);
        }
        // sum(x) >= 1 (i.e. the antecedent of the rule is not empty)
        model.addClausesBoolOrArrayEqualTrue(x);
        // sum(y) >= 1 (i.e. the consequent of the rule is not empty)
        model.addClausesBoolOrArrayEqualTrue(y);
        // Frequency of z
        IntVar freqZ = model.intVar("freqZ", 1, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freqZ, z).post();
        // Frequency of x
        IntVar freqX = model.intVar("freqX", 1, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freqX, x).post();
        // Frequency of y
        IntVar freqY = model.intVar("freqY", 1, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freqY, y).post();
        // Ensure that each rule is generated from a skypattern
        BoolVar[] skyVar = skypatternConstraint(y, z, skypatterns, database, model);
        // Ensure that y is a class item

        Solver solver = model.getSolver();
        // Search strategy : first, instantiate skyVar, then x, then y, then z
        solver.setSearch(intVarSearch(
                new InputOrder<>(model),
                new IntDomainMin(),
                ArrayUtils.append(skyVar, x, y, z)
        ));
        System.out.println("List of the rules extracted from the skypatterns:");
        while (solver.solve()) {
            double conf = (double) freqZ.getValue() / freqX.getValue();
            double lift = conf * database.getNbTransactions() / freqY.getValue();
            System.out.println(Arrays.toString(convertItemsetToLabel(irisLabels, x)) + " => " +
                    Arrays.toString(convertItemsetToLabel(irisLabels, y)) + ", freq=" + freqZ.getValue() + ", conf=" + conf + ", lift=" + lift);
        }
        System.out.println("______________________");
        solver.printStatistics();
    }

    public static void main(String[] args) throws Exception {
        modelToExtractRules();
    }
}
