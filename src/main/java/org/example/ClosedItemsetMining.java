package org.example;

import io.gitlab.chaver.mining.patterns.constraints.factory.ConstraintFactory;
import io.gitlab.chaver.mining.patterns.io.DatReader;
import io.gitlab.chaver.mining.patterns.io.TransactionalDatabase;
import io.gitlab.chaver.mining.patterns.search.strategy.selectors.variables.MinCov;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import java.util.Arrays;

import static util.TPUtil.*;

/**
 * Closed Itemset Mining : the goal is to find all the itemsets that have a frequency greater than a given theta threshold
 * and that are closed with respect to the frequency.
 * We say that an itemset x is closed with respect to the frequency iff there exists no itemset y such that
 * x is a subset of y and freq(x) = freq(y)
 */
public class ClosedItemsetMining {

    public static void main(String[] args) throws Exception {
        String[] irisLabels = readLabels("data/iris.names");
        String dataPath = "data/iris.dat";
        int theta = 10;
        Model model = new Model("Closed Itemset Mining");
        TransactionalDatabase database = new DatReader(dataPath).read();
        // Array of Boolean variables where x[i] is true iff item i belongs to the itemset
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        // Integer variable that represents the frequency of x
        IntVar freq = model.intVar("freq", theta, database.getNbTransactions());
        // CoverSize is a constraint that links the freq variable to the frequency of x
        ConstraintFactory.coverSize(database, freq, x).post();
        // Post the constraint to ensure that x is a closed itemset

        // Set up the search strategy
        Solver solver = model.getSolver();
        solver.setSearch(Search.intVarSearch(
                new MinCov(model, database),
                new IntDomainMin(),
                x
        ));
        System.out.println("List of closed itemsets for the dataset " + dataPath + " with a frequency threshold=" + theta + ":");
        while (solver.solve()) {
            String[] itemset = convertItemsetToLabel(irisLabels, x);
            System.out.println("x=" + Arrays.toString(itemset) + ", freq(x)=" + freq.getValue());
        }
        System.out.println("______________________");
        solver.printStatistics();
    }
}
