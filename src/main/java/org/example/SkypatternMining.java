package org.example;

import io.gitlab.chaver.mining.patterns.constraints.factory.ConstraintFactory;
import io.gitlab.chaver.mining.patterns.io.DatReader;
import io.gitlab.chaver.mining.patterns.io.TransactionalDatabase;
import io.gitlab.chaver.mining.patterns.measure.Measure;
import io.gitlab.chaver.mining.patterns.measure.operand.MeasureOperand;
import io.gitlab.chaver.mining.patterns.search.strategy.selectors.variables.MinCov;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


import static util.TPUtil.*;
import static io.gitlab.chaver.mining.patterns.measure.MeasureFactory.*;

/**
 * Skypattern mining: Given a set of measures M, the goal is to find all the Pareto-dominant itemsets w.r.t. M
 */
public class SkypatternMining {

    public static void main(String[] args) throws Exception {
        String[] irisLabels = readLabels("data/iris.names");
        String dataPath = "data/iris.dat";
        Model model = new Model("Skypattern Mining");
        TransactionalDatabase database = new DatReader(dataPath).read();
        // Array of Boolean variables where x[i] is true iff item i belongs to the itemset
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        // Integer variable that represents the frequency of x
        IntVar freq = model.intVar("freq", 1, database.getNbTransactions());
        // CoverSize is a constraint that links the freq variable to the frequency of x
        ConstraintFactory.coverSize(database, freq, x).post();
        // Integer variable that represents the length (also called size) of x
        IntVar length = model.intVar("length", 2, database.getNbItems());
        // Post a constraint sum to ensure that length(x) = sum(x_i)
        model.sum(x, "=", length).post();
        // Integer variable that represents the area of x
        IntVar area = freq.mul(length).intVar();
        // itemFreq[i] is an array of integer variables such that:
        // - itemFreq[i] = freq(i) if x[i] = 1
        // - itemFreq[i] = 0 if x[i] = 0
        IntVar[] itemFreq = model.intVarArray(x.length, 0, database.getNbTransactions());
        int[] freqItems = database.computeItemFreq();
        for (int i = 0; i < x.length; i++) {
            itemFreq[i].eq(x[i].mul(freqItems[i])).post();
        }
        // Integer variable that represents max(x.freq)
        IntVar maxFreq = model.intVar("maxFreq", 0, database.getNbTransactions());
        // Post a constraint to ensures that maxFreq = max(itemFreq)
        model.max(maxFreq, itemFreq).post();
        // We compute the value of aconf(x) = freq(x) / max(x.freq)
        IntVar aconf = freq.mul(10000).div(maxFreq).intVar();
        // Now we are going to post two constraints
        List<Measure> M = Arrays.asList(freq(), area(), allConf());
        // First, we need to post a constraint to ensures that x is closed w.r.t. M', where M' is a set of measures such that M is maximally M'-skylineable
        Set<Measure> M_prime = MeasureOperand.maxConvert(M);
        System.out.println(M + " is maximally " + M_prime + "-skylineable");
        System.out.println("");
        ConstraintFactory.adequateClosure(database, new ArrayList<>(M_prime), x, false).post();
        // Set up the search strategy
        Solver solver = model.getSolver();
        solver.setSearch(Search.intVarSearch(
                new MinCov(model, database),
                new IntDomainMin(),
                x
        ));
        System.out.println("List of skypatterns w.r.t. M = {freq(x),area(x),aconf(x)} for the dataset " + dataPath + ":");
        // Extract all the skypatterns (Pareto front) and print them

        System.out.println("______________________");
        solver.printStatistics();
    }
}
